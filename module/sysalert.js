/**
 * 系统弹出层组件
 * @author johnny
 * @date 2020/8/18 16:28
 */
layui.define(['form', 'layer','component'], function (exports) {
    var $ = layui.$
        , form = layui.form
        , layer = layui.layer
        , component = layui.component

    var obj = {
        /**
         *
         sysalert.render({
                title: '请填写信息'
                , btnYes: '提交'
                , msg: '我是提示文字'
                , msgStyle: 'color: red'
                , closeWin: true // 完成之后关掉窗口,默认
                , list: [
                    {labelName: '姓名', name: 'name', verify: 'required', xx: true, readonly: true, value: '张三'}
                    , {labelName: '密码', name: 'pwd', type: 'password'}
                    , {labelName: '年龄', name: 'age', placeholder: '只能填写整数'}
                    , {labelName: '时间', name: 'time', verify: 'required', xx: true}
                    , {
                        labelName: '性别', name: 'sex', type: 'select', value: '2', list: [
                            {code: '1', name: '男'}, {code: '2', name: '女'}
                        ]
                    }
                    , {labelName: '开关', name: 'kaiguan', type: 'switch', text: '开|关', checked: false}
                    , {
                        labelName: '单选', name: 'radioSex', type: 'radio', value: '2', list: [
                            {code: '1', name: '男'}, {code: '2', name: '女'}, {code: '3', name: '未知'}
                        ]
                    }
                    , {labelName: '备注', name: 'remark', type: 'textarea', value: 'aaaaaa', maxlength: 10}

                ]
                , success(idprefix) {
                    laydate.render({
                        elem: '#' + idprefix + 'time'
                        , type: 'datetime'
                    });
                }
                // ,done(res){
                //     console.log(res);
                // }
        });
         */
         render

    };

    function render(params) {
        params = params || {};

        if (params.closeWin == null || params.closeWin == undefined || typeof params.closeWin != "boolean") {
            params.closeWin = true;
        }
        if (params.showButYes == null || params.showButYes == undefined || typeof params.showButYes != "boolean") {
            params.showButYes = true;
        }
        params.title = params.title || '信息';
        params.btnYes = params.btnYes || '确认';
        params.btnCancel = params.btnCancel || '取消';
        params.list = params.list || [];
        params.success = params.success || function (id) {
        }
        params.done = params.done || function (d) {
            layer.msg(JSON.stringify(d));
        }
        params.msgStyle = params.msgStyle || '';
        params.area = params.area || '400px';

        var time = new Date().getTime() + '';
        var idprefix = 'id_' + time + '_';
        var promptIndex = layer.open({
            title: params.title
            , type: 1
            , shadeClose: params.shadeClose
            , area: params.area
            // , area: ['400px', '450px'] //宽,高
            , content: promptHtml(params, idprefix)
            , success() {

                for (let l of params.list) {
                    var id = idprefix + l.name;
                    var othis = $("#" + id);
                    if (l.value) {
                        othis.val(l.value);
                    }
                    if (l.readonly == true) {
                        if (l.type == 'text' || l.type == 'password') {
                            othis.addClass("layui-bg-gray");
                            othis.attr("readonly", "");
                        } else if (l.type == 'select') {
                            othis.addClass("layui-btn-disabled");
                            othis.attr("disabled", "");
                        }
                    }
                    // 开关的监听方法
                    if (l.type == 'switch') {
                        form.on('switch(' + l.name + ')', function (data) {
                            $("#" + idprefix + l.name).val(data.elem.checked);
                        });
                    }
                }
                params.success(idprefix);

                $(".required-class").each(function () {
                    $(this).prepend('<i class="xx">*&nbsp;</i>');
                });
                form.render();
                $("#promptCancel").click(function () {
                    layer.close(promptIndex);
                });

                form.on('submit(promptYes)', function (data) {
                    if (params.closeWin) {
                        layer.close(promptIndex);
                    }
                    params.done(data.field, promptIndex);
                });
            }
        });
    }
    function promptHtml(params, idprefix) {
        var html = '<div class="layui-form" lay-filter="promptForm" style="padding: 20px 0 0 0;">';
        if (params.msg) {
            html += '<div style="margin: 0px 0px 10px 20px;"><div style="' + params.msgStyle + '">' + params.msg + '</div></div>';
        }

        for (let l of params.list) {
            l.xx = l.xx || false;
            l.type = l.type || 'text';
            l.placeholder = l.placeholder || '';
            l.value = l.value || '';
            if (l.readonly == null || l.readonly == undefined || l.readonly == '') {
                l.readonly = false;
            }
            if (l.type == 'switch') {
                l.text = l.text || '';
                if (l.checked == null || l.checked == undefined || l.checked == '') {
                    l.checked = false;
                }
                if (l.checked) {
                    l.checkedStr = ' checked ';
                } else {
                    l.checkedStr = ' ';
                }
            }

            var id = idprefix + l.name;
            var formElem = '';
            var formElemVal = '';
            var idname = ' id="' + id + '" name="' + l.name + '" ';
            var verify = '';
            if (l.verify) {
                verify = ' lay-verify="' + l.verify + '" ';
            }
            var idnameverify = idname + verify + ' placeholder="' + l.placeholder + '" ';
            var textFoot = idnameverify + ' class="layui-input"/>';

            var maxlengthHtml = '';
            if (l.maxlength) {
                maxlengthHtml = 'maxlength="' + l.maxlength + '"';
            }
            if (l.type == 'text') {
                formElem = '<input type="text" ' + textFoot;
            } else if (l.type == 'password') {
                formElem = '<input type="password" ' + textFoot;
            } else if (l.type == 'textarea') {
                formElem = '<textarea ' + idnameverify + maxlengthHtml + '  class="layui-textarea"></textarea>';
            } else if (l.type == 'switch') {
                formElem = ' <input  lay-filter="' + l.name + '" type="checkbox" lay-skin="switch" lay-text="' + l.text + '" ' + l.checkedStr + ' />';
                formElemVal = '<input type="hidden" ' + idname + ' value="' + l.checked + '"  />';
            } else if (l.type == 'radio') {
                for (let t of l.list) {
                    var radioHtml = ' <input ' + idname + ' type="radio"  value="' + t.code + '" title="' + t.name + '"';
                    if (t.code == l.value) {
                        radioHtml += ' checked />';
                    } else {
                        radioHtml += ' />';
                    }
                    formElem += radioHtml;
                }
            } else if (l.type == 'select') {
                if (l.list) {
                    formElem = '<select ' + idnameverify + '">';
                    for (let s of l.list) {
                        formElem += '<option value="' + s.code + '">' + s.name + '</option>';
                    }
                    formElem += '</select>';
                }
            }
            var xx = l.xx ? 'required-class' : '';
            var f =
                '    <div class="layui-form-item">\n' +
                '        <div class="layui-inline">\n' +
                '            <label class="layui-form-label ' + xx + '">' + l.labelName + '</label>\n' +
                '            <div class="layui-input-inline">' + formElem + '</div>\n' +
                '        </div>\n' +
                '    </div>' + formElemVal;

            html += f;
        }
        var yesHtml = '';
        if (params.showButYes) {
            yesHtml = '   <button class="layui-btn layui-btn-sm layui-btn-normal" lay-submit  lay-filter="promptYes" >' + params.btnYes + '</button>\n';
        }

        // 定义 提交和取消按钮的位置样式
        var btnStyle = 'class="layui-layer-btn"';
        if (params.btnStyle) {
            btnStyle = 'style="' + params.btnStyle + '"';
            // 参考    , btnStyle: 'position: absolute;right: 10px;bottom: 10px;'
        }
        html +=
            '<div ' + btnStyle + ' >' + yesHtml +
            '   <button class="layui-btn layui-btn-sm layui-btn-primary" id="promptCancel">' + params.btnCancel + '</button>\n' +
            '</div>';
        html += '</div>'
        return html;
    }


    exports('sysalert', obj);
});
